module.exports = {
  port: process.env.PORT || 8081,
  db: {
    database: process.env.DB_NAME || 'workscout',
    user: process.env.DB_USER || 'workscout',
    password: process.env.DB_PASS || 'workscout',
    options: {
      dialect: process.env.DIALECT || 'sqlite',
      host: process.env.HOST || 'localhost',
      storage: process.env.HOST || './workscout.sqlite'
    }
  },
  elastic_search_host: process.env.ELASTIC_SEARCH_HOST || 'localhost:9200',
  authentication: {
    jwtSecret: process.env.JWT_SECRET || 'secret'
  }
}
