const Joi = require('joi')

module.exports = {
  register (req, res, next) {
    const schema = Joi.object().options({ abortEarly: false }).keys({
      username: Joi.string().alphanum().min(3).max(30).required().label('Username'),
      email: Joi.string().email().required().label('Email Address'),
      password: Joi.string().min(8).required().label('Password'),
      password_confirmation: Joi.any().valid(Joi.ref('password')).required()
        .options({ language: { any: { allowOnly: 'must match password' } } })
        .label('Password Confirmation')
    })

    const {error} = Joi.validate(req.body, schema)

    if (error) {
      res.status(400).send(global.sendJoiErrors(error))
    } else {
      next()
    }
  },
  login (req, res, next) {
    const schema = Joi.object().options({ abortEarly: false }).keys({
      username: Joi.string().alphanum().min(3).max(30).required().label('Username'),
      password: Joi.string().min(8).required().label('Password')
    })

    const {error} = Joi.validate(req.body, schema)

    if (error) {
      res.status(400).send(global.sendJoiErrors(error))
    } else {
      next()
    }
  }
}
