const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const {sequelize} = require('./models')
const config = require('./config/config')
const documents = require('./routes/documents')

const app = express()

app.use(morgan('combine'))
app.use(bodyParser.json())
app.use(cors())
app.use('/documents', documents)

require('./routes/routes')(app)

sequelize.sync()
  .then(() => {
    app.listen(config.port)
    console.log(`Server started on port ${config.port}`)
  })

// Functions

global.sendJoiErrors = function (res) {
  let errors = []

  res.details.forEach((key, value) => {
    errors.push({
      key: key.context.key,
      message: key.message.replace(new RegExp('"', 'g'), '')
    })
  })

  return {errors: errors}
}

global.sendSequelizeErrors = function (res) {
  let errors = []

  res.errors.forEach((k, v) => {
    errors.push({
      key: k.path,
      message: k.message.trim().charAt(0).toUpperCase() + k.message.slice(1)
    })
  })

  return {errors: errors}
}
