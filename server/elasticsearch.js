const elasticsearch = require('elasticsearch')
const config = require('./src/config/config')

const elasticClient = new elasticsearch.Client({
  host: config.elastic_search_host,
  log: 'info'
})

const indexName = 'jobsindex'

/**
 * Delete an existing index
 */
function deleteIndex () {
  return elasticClient.indices.delete({
    index: indexName
  })
}
exports.deleteIndex = deleteIndex

/**
 * create the index
 */

function initIndex () {
  return elasticClient.indices.create({
    index: indexName
  })
}
exports.initIndex = initIndex

/**
 * check if the index exists
 */
function indexExists () {
  return elasticClient.indices.exists({
    index: indexName
  })
}

exports.indexExists = indexExists

function initMapping () {
  return elasticClient.indices.putMapping({
    index: indexName,
    type: 'document',
    body: {
      properties: {
        title: { type: 'string' },
        content: { type: 'string' }
      }
    }
  })
}
exports.initMapping = initMapping

function addDocument (document) {
  console.log(document)
  return elasticClient.index({
    index: indexName,
    type: 'document',
    body: {
      title: document.title,
      content: document.content,
      suggest: {
        input: document.title.split(' '),
        output: document.title,
        payload: document.metadata || {}
      }
    }
  })
}
exports.addDocument = addDocument

function getSuggestions (input) {
  return elasticClient.search({
    index: indexName,
    body: {
      query: {
        match_phrase: {
          title: '.*' + input + '.*'
        }
      }
    }
  })
}
exports.getSuggestions = getSuggestions
