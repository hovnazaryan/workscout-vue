// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Notifications from 'vue-notification'
import VeeValidate from 'vee-validate'
import { sync } from 'vuex-router-sync'
import store from '@/store/store'

Vue.config.productionTip = false

Vue.use(Notifications)
Vue.use(VeeValidate)

sync(store, router)
/* eslint-disable no-new */

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isUserLoggedIn) {
      next({
        path: '/login',
        query: {
          redirect: 'Forbidden'
        }
      })
    } else {
      next()
    }
  } else if ((to.name === 'login' || to.name === 'register') && store.getters.isUserLoggedIn) {
    next({
      path: '/',
      query: {
        redirect: 'Logged In'
      }
    })
  } else {
    next()
  }
})
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
