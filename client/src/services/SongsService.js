import Api from '@/services/Api'

export default {
  index () {
    return Api().get('songs')
  },
  show (songId) {
    return Api().get(`song/${songId}`)
  },
  store (song) {
    return Api().post('songs', song)
  }
}
