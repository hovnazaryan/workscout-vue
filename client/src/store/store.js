import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const LOGIN = 'LOGIN'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const LOGOUT = 'LOGOUT'

export default new Vuex.Store({
  strict: true,
  state: {
    pending: false,
    token: localStorage.getItem('token'),
    user: localStorage.getItem('user'),
    isUserLoggedIn: !!localStorage.getItem('token')
  },
  mutations: {
    [LOGIN] (state) {
      state.pending = true
    },
    [LOGIN_SUCCESS] (state) {
      state.isUserLoggedIn = true
      state.pending = false
    },
    [LOGOUT] (state) {
      state.isUserLoggedIn = false
    }
  },
  actions: {
    login ({commit}, credentials) {
      commit(LOGIN)
      return new Promise(resolve => {
          localStorage.setItem('token', credentials.token)
          localStorage.setItem('user', credentials.user)
          commit(LOGIN_SUCCESS)
          resolve()
      })
    },
    logout ({commit}) {
      localStorage.removeItem('token')
      localStorage.removeItem('user')
      commit(LOGOUT)
    }
  },
  getters: {
    isUserLoggedIn: state => {
      return state.isUserLoggedIn
    }
  }
})
