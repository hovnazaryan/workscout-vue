import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Register from '@/components/Register'
import Login from '@/components/Login'
import Songs from '@/components/Songs'
import CreateSong from '@/components/CreateSong'
import ViewSong from '@/components/ViewSong'
import PageNotFound from '@/components/PageNotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'root',
      meta: {requiresAuth: null},
      component: Index
    },
    {
        path: '/register',
        name: 'register',
        meta: {requiresAuth: false},
        component: Register
    },
    {
      path: '/login',
      name: 'login',
      meta: {requiresAuth: false},
      component: Login
    },
    {
      path: '/songs',
      name: 'songs',
      meta: {requiresAuth: true},
      component: Songs
    },
    {
      path: '/songs/create',
      name: 'create-song',
      meta: {requiresAuth: true},
      component: CreateSong
    },
    {
      path: '/song/:songId',
      name: 'song',
      meta: {requiresAuth: true},
      component: ViewSong
    },
    {
      path: '*',
      component: PageNotFound
    }
  ]
})
